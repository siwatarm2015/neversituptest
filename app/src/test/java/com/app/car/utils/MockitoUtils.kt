package com.app.car.utils

import org.mockito.Mockito.*
import org.mockito.stubbing.OngoingStubbing

inline fun <reified T> mock(): T = mock(T::class.java)
fun <T> whenever(methodCall: T) : OngoingStubbing<T> =
    `when`(methodCall)