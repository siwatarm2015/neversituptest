package com.app.car.presentation.ui.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.app.car.utils.TestCoroutineRule
import com.app.car.domain.Result.Success
import com.app.car.domain.model.users.UsersResponse
import com.app.car.domain.usecase.home.GetCustomersUseCase
import com.app.car.utils.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class CustomerViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var getCustomersUseCase: GetCustomersUseCase

    private lateinit var customerViewModel: CustomerViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getCustomersUseCase = Mockito.mock(GetCustomersUseCase::class.java)
        customerViewModel = CustomerViewModel(getCustomersUseCase)
    }

    @Test
    fun getUserById() {
        runBlockingTest {
            val result = mock<MutableStateFlow<Result<UsersResponse>>>()
            customerViewModel.usersResponse.map { result }
            customerViewModel.getUsersById(id = "10")
            result.collect { data ->
                Success(
                    data = data
                )
                verify(result) {
                    Success(
                        data = it
                    )
                }
            }
        }
    }
}