package com.app.car.presentation.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.app.car.utils.TestCoroutineRule
import com.app.car.domain.Result.*
import com.app.car.domain.model.users.UsersResponse
import com.app.car.domain.usecase.home.GetCustomersUseCase
import com.app.car.utils.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var getCustomersUseCase: GetCustomersUseCase

    private lateinit var homeViewModel: HomeViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getCustomersUseCase = Mockito.mock(GetCustomersUseCase::class.java)
        homeViewModel = HomeViewModel(getCustomersUseCase)
    }

    @Test
    fun getUser() {
        runBlockingTest {
            val result = mock<MutableStateFlow<Result<MutableList<UsersResponse>>>>()
            homeViewModel.usersResponse.map { result }
            homeViewModel.getUsers()
            result.collect { data ->
                Success(data)
                verify(data) {
                    Success(it)
                }
            }
        }
    }
}