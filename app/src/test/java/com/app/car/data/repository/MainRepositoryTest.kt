package com.app.car.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.app.car.utils.TestCoroutineRule
import com.app.car.data.datasource.local.LocalDataSource
import com.app.car.data.datasource.remote.RemoteDataSource
import com.app.car.domain.BaseResponse
import com.app.car.domain.Result.Success
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import org.junit.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainRepositoryTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val remoteDataSource = mock(RemoteDataSource::class.java)

    private val localDataSource = mock(LocalDataSource::class.java)

    private val mainRepo = MainRepository(remoteDataSource, localDataSource)

    @Test
    fun `getUsers - success`() {
        testCoroutineRule.runBlockingTest {
            `when`(
                remoteDataSource.getCustomer()
            ).thenReturn(
                flow {
                    Success(
                        data = BaseResponse(
                            msg = "Success",
                            statusCode = 200,
                            error = "",
                            response = null
                        )
                    )
                }
            )
            mainRepo.getUsers().collect { result ->
                result as Success
                verify(remoteDataSource, times(1)).getCustomer()
                assertNotNull(result.data)
            }
        }
    }

    @Test
    fun `getUsers - failed`() {
        testCoroutineRule.runBlockingTest {
            `when`(
                remoteDataSource.getCustomer()
            ).thenReturn(
                flow {
                    Success(
                        data = BaseResponse(
                            msg = "Success",
                            statusCode = 200,
                            error = "",
                            response = null
                        )
                    )
                }
            )
            mainRepo.getUsers().collect { result ->
                result as Success
                verify(remoteDataSource, times(1)).getCustomer()
                assertNotNull(result.data)
            }
        }
    }
}


