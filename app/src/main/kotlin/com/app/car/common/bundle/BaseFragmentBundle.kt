package com.app.car.common.bundle

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BaseFragmentBundle(
    var msg: String = "",
    var data: String = ""
) : Parcelable
