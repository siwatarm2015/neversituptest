package com.app.car.common.bundle

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CustomerFragmentBundle(
    var token: String = "",
    var customerId: String = ""
) : Parcelable
