package com.app.car.utils

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Base64
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class UtilsPicture {
    companion object {

        const val REQUEST_CODE_TAKE_PHOTO = 4
        const val REQUEST_CODE_CROP_IMAGE = 5
        const val REQUEST_CODE_PICK_GALLERY = 6
        var isSelectGallery: Boolean = false
        private lateinit var imagesFolder: File
        var imagesFile: File? = null
        lateinit var uriSavedImage: Uri

        fun convertBitmaptoBase64(bitmap: Bitmap?): String? {
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            return Base64.encodeToString(byteArray, Base64.NO_WRAP)
        }

//        fun selectPicture(
//            mActivity: FragmentActivity,
//            fragment: Fragment
//        ) {
//            val alertDialog = AlertDialog.Builder(mActivity).create()
//            val binding =
//                DialogPictureChooserBinding.inflate(LayoutInflater.from(mActivity), null, false)
//            alertDialog.setView(binding.root)
//            alertDialog.show()
//            alertDialog.setFullWidth()
//
//            val onClickListener = View.OnClickListener {
//                when (it) {
//                    binding.viewCamera -> {
//                        isSelectGallery = false
//                        checkPermissionCameraAndReadWriteExternalStore(mActivity, fragment)
//                        alertDialog.dismiss()
//                    }
//                    binding.viewPic -> {
//                        isSelectGallery = true
//                        checkPermissionCameraAndReadWriteExternalStore(mActivity, fragment)
//                        alertDialog.dismiss()
//                    }
//                    binding.btnCancel -> {
//                        alertDialog.dismiss()
//                    }
//                }
//            }
//            binding.viewCamera.setOnClickListener(onClickListener)
//            binding.viewPic.setOnClickListener(onClickListener)
//            binding.btnCancel.setOnClickListener(onClickListener)
//        }

        fun checkPermissionCameraAndReadWriteExternalStore(
            mActivity: FragmentActivity,
            fragment: Fragment
        ) {
            Dexter.withContext(mActivity)
                .withPermissions(ArrayList<String>().apply {
                    this.add(Manifest.permission.CAMERA)
                    this.add(Manifest.permission.READ_EXTERNAL_STORAGE)
                    this.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                })
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted()) {
//                            setDestinationFolderCaptureImage(mActivity)
                            if (isSelectGallery) {
                                startIntentGallery(fragment)
                            } else {
                                startIntentCapture(fragment, mActivity)
                            }
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>?,
                        token: PermissionToken?
                    ) {
                        token!!.continuePermissionRequest()
                    }
                })
                .check()
        }

        fun startIntentGallery(
            fragment: Fragment
        ) {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            intent.type = "image/*"
            fragment.startActivityForResult(intent, REQUEST_CODE_PICK_GALLERY)
        }

        fun startIntentCapture(
            fragment: Fragment,
            mActivity: FragmentActivity
        ) {
            try {
                imagesFile = createImageFile(mActivity)
            } catch (ex: IOException) {
                ex.printStackTrace()
            }

            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(mActivity.packageManager)?.also {
                    if (imagesFile != null) {
                        uriSavedImage = FileProvider.getUriForFile(
                            mActivity,
                            mActivity.packageName + ".provider",
                            imagesFile!!
                        )
                    }
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage)
                    fragment.startActivityForResult(takePictureIntent, REQUEST_CODE_TAKE_PHOTO)
                }
            }


//            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage)
//            fragment.startActivityForResult(cameraIntent, REQUEST_CODE_TAKE_PHOTO)
        }

        private fun setDestinationFolderCaptureImage(mActivity: FragmentActivity) {
            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            builder.detectFileUriExposure()
            imagesFolder = File(mActivity.getExternalFilesDir("imageProfile"), "/TrafficReporter")
            imagesFolder.mkdirs()
        }

        @Throws(IOException::class)
        fun createImageFile(activity: FragmentActivity): File? { // Create an image file name
            val timeStamp =
                SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(Date())
            val mFileName = "TrafficReporter_${timeStamp}_"
            val storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            return File.createTempFile(mFileName, ".jpg", storageDir)
        }
    }
}
