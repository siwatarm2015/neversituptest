package com.app.car.utils.textwatcher

import android.content.Context
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText

class FormatEditText : TextInputEditText {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
    }

    fun setDashFormat(format: String?) {
        if (format != null) {
            addTextChangedListener(SpaceTextWatcher(this, format))
            val keyListener = DigitsKeyListener.getInstance("0123456789" + FormatUtility.SEPARATE)
            setKeyListener(keyListener)
        }
    }
}
