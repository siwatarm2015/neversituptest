package com.app.car.utils

import android.content.Context
import android.graphics.Paint
import android.graphics.drawable.RippleDrawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.app.car.R

class RippleTextView : AppCompatTextView {
    private var rippleEnabled: Boolean = true
    private var ribBackgroundColor: Int = 0
    private var rippleColor: Int = 0
    private var radius = pxFromDp(12)
    private var shapeRippleType = ShapeRippleType.Rectangle

    enum class ShapeRippleType {
        Oval,
        Rectangle
    }

    private val rippleDrawable: RippleDrawable? = null
    private val paint: Paint? = null

    constructor(context: Context) : super(context) {
        // Retrieve attribute values at runtime
        getXMLAttributes(context, null)
        RippleEffect.addRippleEffect(
            this,
            rippleEnabled,
            ribBackgroundColor,
            rippleColor,
            radius,
            shapeRippleType
        )
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        // Retrieve attribute values at runtime
        getXMLAttributes(context, attrs)
        RippleEffect.addRippleEffect(
            this,
            rippleEnabled,
            ribBackgroundColor,
            rippleColor,
            radius,
            shapeRippleType
        )
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        // Retrieve attribute values at runtime
        getXMLAttributes(context, attrs)
        RippleEffect.addRippleEffect(
            this,
            rippleEnabled,
            ribBackgroundColor,
            rippleColor,
            radius,
            shapeRippleType
        )
    }

    private fun getXMLAttributes(context: Context, attrs: AttributeSet?) {
        val typedArray = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.RippleText,
            0, 0
        )
        try {
            rippleEnabled = typedArray.getBoolean(
                R.styleable.RippleText_rippleEnabled,
                true
            )
            radius = typedArray.getDimensionPixelSize(
                R.styleable.RippleText_ribRadiusRound, pxFromDp(12).toInt()
            ).toFloat()
            ribBackgroundColor = typedArray.getColor(
                R.styleable.RippleText_ribBackgroundColor,
                ContextCompat.getColor(context, R.color.background)
            )
            rippleColor = typedArray.getColor(
                R.styleable.RippleText_rippleColor,
                ContextCompat.getColor(context, R.color.ripple_default)
            )
            val shape =
                typedArray.getInt(R.styleable.RippleText_shape_ripple, shapeRippleType.ordinal)
            for (s in ShapeRippleType.values()) {
                if (s.ordinal == shape) {
                    shapeRippleType = s
                    break
                }
            }
        } finally {
            typedArray.recycle()
        }
    }

    private fun pxFromDp(dp: Int): Float {
        return dp * context.resources.displayMetrics.density
    }

    fun getRippleColor(): Int {
        return rippleColor
    }

    fun setRippleColor(rippleColor: Int) {
        this.rippleColor = rippleColor
        invalidate()
    }
}
