package com.app.car.utils

import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.StateListDrawable
import android.graphics.drawable.shapes.OvalShape
import android.graphics.drawable.shapes.RectShape
import android.graphics.drawable.shapes.RoundRectShape
import android.os.Build
import android.util.StateSet
import android.view.View
import android.widget.FrameLayout
import androidx.core.view.ViewCompat

object RippleEffect {

    fun addRippleEffect(
        view: View,
        rippleEnabled: Boolean,
        backgroundColor: Int,
        rippleColor: Int,
        radius: Float,
        shapeType: RippleTextView.ShapeRippleType
    ) {
        if (rippleEnabled && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Create RippleDrawable
            if (view is FrameLayout) {
                view.foreground =
                    getPressedColorRippleDrawable(backgroundColor, rippleColor, shapeType, radius)
            } else {
                view.background =
                    getPressedColorRippleDrawable(backgroundColor, rippleColor, shapeType, radius)
            }

            // Customize Ripple color
            val rippleDrawable: RippleDrawable = if (view is FrameLayout) {
                view.foreground as RippleDrawable
            } else {
                view.background as RippleDrawable
            }
            val states = arrayOf(intArrayOf(android.R.attr.state_enabled))
            val colors = intArrayOf(rippleColor)
            val colorStateList = ColorStateList(states, colors)
            rippleDrawable.setColor(colorStateList)
            if (view is FrameLayout) {
                view.foreground = rippleDrawable
            } else {
                view.background = rippleDrawable
            }
        } else if (rippleEnabled) {
            // Create Selector for pre Lollipop
            ViewCompat.setBackground(view, createStateListDrawable(backgroundColor, rippleColor))
        }
        // else {
        //     // Ripple Disabled
        //     // view.background = ColorDrawable(backgroundColor)
        // }
    }

    private fun createStateListDrawable(backgroundColor: Int, rippleColor: Int): StateListDrawable {
        val stateListDrawable = StateListDrawable()
        stateListDrawable.addState(
            intArrayOf(android.R.attr.state_pressed),
            createDrawable(rippleColor)
        )
        stateListDrawable.addState(StateSet.WILD_CARD, createDrawable(backgroundColor))
        return stateListDrawable
    }

    private fun createDrawable(background: Int): Drawable {
        val shapeDrawable = ShapeDrawable(RectShape())
        shapeDrawable.paint.color = background

        return shapeDrawable
    }

    private fun getPressedColorRippleDrawable(
        normalColor: Int,
        pressedColor: Int,
        shapeType: RippleTextView.ShapeRippleType,
        radius: Float
    ): Drawable {
        return RippleDrawable(
            getPressedColorSelector(normalColor, pressedColor),
            null,
            getRippleMask(normalColor, shapeType, radius)
        )
    }

    private fun getRippleMask(
        color: Int,
        shapeType: RippleTextView.ShapeRippleType,
        radius: Float
    ): Drawable {
        val outerRadii =
            floatArrayOf(radius, radius, radius, radius, radius, radius, radius, radius)
        val shapeDrawable = if (shapeType == RippleTextView.ShapeRippleType.Rectangle) {
            ShapeDrawable(RoundRectShape(outerRadii, null, null))
        } else {
            ShapeDrawable(OvalShape())
        }
        // val shapeDrawable = ShapeDrawable(r)
        shapeDrawable.paint.color = color
        return shapeDrawable
    }

    private fun getPressedColorSelector(normalColor: Int, pressedColor: Int): ColorStateList {
        return ColorStateList(
            arrayOf(intArrayOf()),
            intArrayOf(pressedColor)
        )
    }

    private fun getColorDrawableFromColor(color: Int): ColorDrawable {
        return ColorDrawable(color)
    }
}
