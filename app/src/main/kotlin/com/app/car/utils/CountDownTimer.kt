package com.app.car.utils

import android.os.CountDownTimer

class CountDownTimer {

    private lateinit var timer: CountDownTimer

    fun startTimer(onTick: (millisUntilFinished: Long) -> Unit, onFinish: () -> Unit) {
        timer = object : CountDownTimer(300000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                onTick(millisUntilFinished)
            }

            override fun onFinish() {
                onFinish()
            }
        }.start()

    }

    fun stopTimer(){
        timer.cancel()
    }

}
