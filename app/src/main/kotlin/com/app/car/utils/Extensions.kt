package com.app.car.utils

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.AnyRes
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jakewharton.rxbinding4.view.clicks
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import java.nio.ByteBuffer
import java.util.*
import java.util.concurrent.TimeUnit


fun ViewGroup.inflateExt(layoutId: Int) =
    LayoutInflater.from(context).inflate(layoutId, this, false)

fun ViewGroup.inflateCustomView(layoutId: Int) =
    LayoutInflater.from(context).inflate(layoutId, null)

fun View.clickWithDebounce(debounceTime: Long = 600L, action: (view: View) -> Unit): Disposable =
    this.clicks().debounce(debounceTime, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread()).subscribe { action(this) }


inline fun <reified T> Gson.toList(json: String) =
    fromJson<List<T>>(
        json,
        ListOfSomething<T>(T::class.java)
    )

inline fun <reified T> Gson.toList(jsonArr: JsonArray) =
    fromJson<List<T>>(
        jsonArr,
        ListOfSomething<T>(T::class.java)
    )

inline fun <reified T> Gson.toList(jsonObject: JsonObject) =
    fromJson<List<T>>(
        jsonObject,
        ListOfSomething<T>(T::class.java)
    )

fun View.enable() {
    this.isEnabled = true
}

fun View.disEnable() {
    this.isEnabled = false
}

fun View.clearBackground() {
    this.background = null
}

fun View.updateBackground(@DrawableRes idRes: Int) {
    this.background = ContextCompat.getDrawable(
        context,
        idRes
    )
}

fun String.clearFormat(format: String): String = this.replace(" ", "")

//// text inout solid
//fun View.updateTextInputSolidBackgroundFocused() {
//    this.background = ContextCompat.getDrawable(
//        context!!,
//        R.drawable.bg_solid_text_input_focused
//    )
//}
//
//fun View.updateTextInputSolidBackgroundNormal() {
//    this.background = ContextCompat.getDrawable(
//        context!!,
//        R.drawable.bg_solid_text_input_normal
//    )
//}
//
//fun View.updateTextInputSolidBackgroundError() {
//    this.background = ContextCompat.getDrawable(
//        context!!,
//        R.drawable.bg_solid_text_input_error
//    )
//}
//
//fun TextView.textColorFocused() =
//    setTextColor(ContextCompat.getColor(context, R.color.colorAzure))
//
//fun TextView.textColorNormal() =
//    setTextColor(ContextCompat.getColor(context, R.color.colorPrussianBlue))
//
//fun TextView.textColorError() =
//    setTextColor(ContextCompat.getColor(context, R.color.colorPinkishRed))
//// text inout fill
////
////fun View.updateTextInputFillBackgroundFocused() {
////    this.background = ContextCompat.getDrawable(
////        context!!,
////        R.drawable.bg_fill_text_input_focused
////    )
////}
////
////fun View.updateTextInputFillBackgroundNormal() {
////    this.background = ContextCompat.getDrawable(
////        context!!,
////        R.drawable.bg_fill_text_input_normal
////    )
////}
////
////fun View.updateTextInputFillBackgroundError() {
////    this.background = ContextCompat.getDrawable(
////        context!!,
////        R.drawable.bg_fill_text_input_error
////    )
////}
//
//fun AppCompatImageView.setTint(@ColorRes color: Int) {
//    this.setColorFilter(ContextCompat.getColor(App.getMContext(), color))
//}
//
//fun AppCompatTextView.updateTextColor(@ColorRes color: Int) {
//    this.setTextColor(ContextCompat.getColor(App.getMContext(), color))
//}

fun AppCompatImageView.clearTint() {
    this.colorFilter = null
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.isVisible() = visibility == View.VISIBLE

fun View.setVisibility(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun TextView.setDrawableStart(@DrawableRes start: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) setCompoundDrawablesRelativeWithIntrinsicBounds(
        start,
        0,
        0,
        0
    )
}

fun TextView.setDrawableTop(@DrawableRes top: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) setCompoundDrawablesRelativeWithIntrinsicBounds(
        0,
        top,
        0,
        0
    )
}

fun TextView.setDrawableEnd(@DrawableRes end: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) setCompoundDrawablesRelativeWithIntrinsicBounds(
        0,
        0,
        end,
        0
    )
}

fun TextView.setDrawableBottom(@DrawableRes bottom: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) setCompoundDrawablesRelativeWithIntrinsicBounds(
        0,
        0,
        0,
        bottom
    )
}

fun View.setBackgroundColorz(@ColorRes resId: Int) =
    setBackgroundColor(ContextCompat.getColor(context, resId))

fun TextView.setTextColorz(@ColorRes resId: Int) =
    setTextColor(ContextCompat.getColor(context, resId))

fun EditText.onTextChanged(text: (String?) -> Unit) = addTextChangedListener(object : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        text(s.toString())
    }
})

val Fragment.requireArguments: Bundle
    get() = arguments ?: throw Exception("No arguments found!")

fun ViewPager.onPageSelected(position: (Int?) -> Unit) {
    addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(p0: Int) {
        }

        override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
        }

        override fun onPageSelected(position: Int) {
            position(position)
        }
    })
}

fun String.capitalizedWord(): String {
    val words = replace('_', ' ').toLowerCase(Locale.getDefault())
        .split(" ".toRegex())
        .dropLastWhile { it.isEmpty() }
        .toTypedArray()
    var aString = ""
    for (word in words) {
        aString = aString + word.substring(
            0,
            1
        ).toUpperCase(Locale.getDefault()) + word.substring(1) + " "
    }
    return aString
}

fun Context.getColorRes(@ColorRes colorId: Int) = ContextCompat.getColor(this, colorId)

inline fun <reified R : Any> Array<*>.findInstance(): R? = find { it is R } as R?

fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
    // Raw height and width of image
    val (height: Int, width: Int) = options.run { outHeight to outWidth }
    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {

        val halfHeight: Int = height / 2
        val halfWidth: Int = width / 2

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
            inSampleSize *= 2
        }
    }

    return inSampleSize
}

fun Bitmap.convertToByteArray(): ByteArray {
    // minimum number of bytes that can be used to store this bitmap's pixels
    val size = this.byteCount

    // allocate new instances which will hold bitmap
    val buffer = ByteBuffer.allocate(size)
    val bytes = ByteArray(size)

    // copy the bitmap's pixels into the specified buffer
    this.copyPixelsToBuffer(buffer)

    // rewinds buffer (buffer position is set to zero and the mark is discarded)
    buffer.rewind()

    // transfer bytes from buffer into the given destination array
    buffer.get(bytes)

    // return bitmap's pixels
    return bytes
}

fun RecyclerView.disableItemAnimator() {
    (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
}

fun Context.dial(tel: String?) = startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + tel)))

fun Context.getResourceUri(@AnyRes resourceId: Int): Uri = Uri.Builder()
    .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
    .authority(packageName)
    .path(resourceId.toString())
    .build()

val <T> T.exhaustive: T
    get() = this
