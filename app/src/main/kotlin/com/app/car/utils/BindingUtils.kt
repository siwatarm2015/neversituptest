package com.app.car.utils

import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.databinding.BindingAdapter

object BindingUtils {

    interface OnKeyPressedListener {
        fun onKeyPressed()
    }

    @JvmStatic
    @BindingAdapter("onKeyDone")
    fun onKeyDone(editText: EditText, action: OnKeyPressedListener) {
        editText.setOnEditorActionListener { _, i, _ ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                action.onKeyPressed()
            }
            false
        }
    }

//    @JvmStatic
//    @BindingAdapter("clickWithDebounce")
//    fun clickWithDebounce(view: View, OnClickWithDebounce: OnClickWithDebounce) {
//        view.clickWithDebounce {
//            OnClickWithDebounce.onClickWithDebounce()
//        }
//    }
//
//    interface OnClickWithDebounce {
//        fun onClickWithDebounce()
//    }
//
//    interface OnClickWithDebounceWithParams {
//        fun onClickWithDebounce(view: View)
//    }
//
//    @JvmStatic
//    @BindingAdapter("clickWithDebounceWithParams")
//    fun clickWithDebounceWithParams(
//        view: View,
//        onClickWithDebounceWithParams: OnClickWithDebounceWithParams
//    ) {
//        view.clickWithDebounce {
//            onClickWithDebounceWithParams.onClickWithDebounce(it)
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter(value = ["imageUrl", "placeholderRes"], requireAll = false)
//    fun loadImage(
//        view: ImageView,
//        url: String?,
//        @DrawableRes placeholderRes: Int = R.drawable.ic_placeholder
//    ) {
//        url?.let {
//            view.load(it) {
//                crossfade(false)
//                    .diskCachePolicy(CachePolicy.ENABLED)
//                    .memoryCachePolicy(CachePolicy.ENABLED)
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter(value = ["imageUri", "placeholderRes"], requireAll = false)
//    fun loadImageUri(
//        view: ImageView,
//        url: String?,
//        @DrawableRes placeholderRes: Int = R.drawable.ic_placeholder
//    ) {
//        url?.let {
//            view.load(getStringToUri(view.context, it)) {
//                crossfade(true)
//                    .memoryCachePolicy(CachePolicy.ENABLED)
//                    .size(100, 100)
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter(value = ["loadImageWithMediaUriCircle", "placeholderRes"], requireAll = false)
//    fun setLoadImageWithMediaUriCircle(
//        view: ImageView,
//        uri: Uri?,
//        @DrawableRes placeholderRes: Int = R.drawable.ic_logo
//    ) {
//        uri?.let {
//            val mmr = MediaMetadataRetriever()
//            mmr.setDataSource(view.context, uri)
//            val pictureArray = mmr.embeddedPicture
//            val drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_logo)
//            val bitmap =
//                if (pictureArray == null) drawable?.toBitmap() else BitmapFactory.decodeByteArray(
//                    pictureArray,
//                    0,
//                    pictureArray.size
//                )
//            view.load(bitmap) {
//                crossfade(true)
//                    .memoryCachePolicy(CachePolicy.ENABLED)
//                    .diskCachePolicy(CachePolicy.ENABLED)
//                    .size(100, 100)
//            }
//            mmr.release()
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter(value = ["loadImageWithByteArray", "placeholderRes"], requireAll = false)
//    fun setLoadImageWithByteArray(
//        view: ImageView,
//        array: ByteArray?,
//        @DrawableRes placeholderRes: Int = R.drawable.ic_logo
//    ) {
//        array?.let {
//            val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size)
//            view.load(bitmap) {
//                crossfade(false)
//                    .size(100, 100)
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter(value = ["loadImageWithDrawableId", "placeholderRes"], requireAll = false)
//    fun setLoadImageWithDrawableId(
//        view: ImageView,
//        @DrawableRes id: Int?,
//        @DrawableRes placeholderRes: Int = R.drawable.ic_logo
//    ) {
//        id?.let {
//            view.load(id) {
//                crossfade(false)
//                    .size(100, 100)
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter(value = ["imageUriCircle", "placeholderRes"], requireAll = false)
//    fun loadImageUriCircle(
//        view: ImageView,
//        url: String?,
//        @DrawableRes placeholderRes: Int = R.drawable.ic_placeholder
//    ) {
//        url?.let {
//            view.load(getStringToUri(view.context, it)) {
//                crossfade(true)
//                    .memoryCachePolicy(CachePolicy.ENABLED)
//                transformations(CircleCropTransformation())
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter(value = ["imageCircleUrl", "placeholderRes"], requireAll = false)
//    fun loadImageCircle(
//        view: ImageView,
//        url: String?,
//        @DrawableRes placeholderRes: Int = R.drawable.ic_placeholder
//    ) {
//        view.load(url) {
//            crossfade(false)
//            error(placeholderRes)
//            placeholder(placeholderRes)
//            transformations(CircleCropTransformation())
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter(value = ["imageCircleBase64", "placeholderRes"], requireAll = false)
//    fun loadImageCircleBase64(
//        view: ImageView,
//        base64: String?,
//        @DrawableRes placeholderRes: Int = R.drawable.ic_placeholder
//    ) {
//        base64?.let {
//            if (it.isNotEmpty()) {
//                val imageBytes = Base64.decode(base64, Base64.DEFAULT)
//                val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
//                view.load(decodedImage) {
//                    crossfade(false)
//                    error(placeholderRes)
//                    placeholder(placeholderRes)
//                    transformations(CircleCropTransformation())
//                }
//            } else {
//                view.load("") {
//                    crossfade(false)
//                    error(placeholderRes)
//                    placeholder(placeholderRes)
//                    transformations(CircleCropTransformation())
//                }
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter(value = ["imageWithBlur", "samplingBlur"], requireAll = false)
//    fun loadImageWithBlur(view: AppCompatImageView, @IdRes res: Int, mSampling: Int?) {
//        view.load(res) {
//            transformations(
//                BlurTransformation(view.context, sampling = mSampling?.toFloat() ?: 20f)
//            )
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter("loadImageWithDrawableRes")
//    fun imageWithDrawableRes(view: ImageView, @DrawableRes id: Int) {
//        view.setImageResource(id)
//    }
//
//    @JvmStatic
//    @BindingAdapter(
//        value = ["loadImageWithNotificationViewType", "drawableFirst", "drawableSecond", "drawableThird", "drawableFourth"],
//        requireAll = false
//    )
//    fun imageWithNotificationViewType(
//        view: ImageView,
//        type: String?,
//        @DrawableRes idFirst: Int,
//        @DrawableRes idSecond: Int,
//        @DrawableRes idThird: Int,
//        @DrawableRes idFourth: Int
//    ) {
//        type?.let {
//            if (it.isNotEmpty()) {
//                view.setImageResource(
//                    when (it) {
//                        NotificationViewType.ELOCKER.value -> idFirst
//                        NotificationViewType.APPOINTMENT.value -> idSecond
//                        NotificationViewType.ANNOUNCEMENT.value -> idThird
//                        NotificationViewType.QUESTIONSNAIRE.value -> idFourth
//                        else -> idFirst
//                    }
//                )
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter("textFormatHtml")
//    fun setTextHtml(view: AppCompatTextView, text: String?) {
//        text?.let {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                view.text = Html.fromHtml(it, Html.FROM_HTML_MODE_LEGACY)
//            } else {
//                view.text = Html.fromHtml(it)
//            }
//        }
//    }
//
//    @SuppressLint("SetTextI18n")
//    @JvmStatic
//    @BindingAdapter("textFormatPhone")
//    fun setTextFormatPhone(view: TextView, text: String?) {
//        text?.let {
//            if (it.length == 10) {
//                view.text =
//                    "${it.substring(0, 3)} ${it.substring(3, 6)} ${it.substring(6, it.length)}"
//            } else {
//                view.text = text
//            }
//        }
//    }
//
//    @SuppressLint("SetTextI18n")
//    @JvmStatic
//    @BindingAdapter("textFormatCitizen")
//    fun setTextFormatCitizen(view: TextView, text: String?) {
//        text?.let {
//            if (it.length == 13) {
//                view.text =
//                    "${it.substring(0, 1)} ${it.substring(1, 5)} ${it.substring(
//                        5,
//                        10
//                    )} ${it.substring(10, 12)} ${it.substring(12, text.length)}"
//            } else {
//                view.text = "-"
//            }
//        }
//    }
//
//    @SuppressLint("SetTextI18n", "SimpleDateFormat")
//    @JvmStatic
//    @BindingAdapter(value = ["timeStampToDateTime", "languageType"], requireAll = true)
//    fun setTimeStampToDateTime(view: AppCompatTextView, timeStamp: String?, languageType: String?) {
//        timeStamp?.let {
//            if (it.isEmpty()) return
//            val c = Calendar.getInstance()
//            c.timeInMillis = it.toLong() * 1000
//            val year = c.get(Calendar.YEAR)
//            val month = c.get(Calendar.MONTH)
//            val day = c.get(Calendar.DATE)
//            val monthText =
//                if (languageType == ChangeLanguageType.ENG.value) view.resources.getStringArray(
//                    R.array.monthListEN
//                )[month] else view.resources.getStringArray(R.array.monthListTH)[month]
//            view.text =
//                "${String.format(
//                    "%02d",
//                    day
//                )} $monthText ${if (languageType == ChangeLanguageType.ENG.value) year else year + 543}  ${String.format(
//                    "%02d:%02d",
//                    c.get(Calendar.HOUR_OF_DAY),
//                    c.get(Calendar.MINUTE)
//                )}"
//        }
//    }
//
//    @SuppressLint("SetTextI18n", "SimpleDateFormat")
//    @JvmStatic
//    @BindingAdapter(value = ["timeStampToDate", "languageType"], requireAll = true)
//    fun setTimeStampToDate(view: AppCompatTextView, timeStamp: String?, languageType: String?) {
//        timeStamp?.let {
//            if (it.isEmpty()) return
//            val c = Calendar.getInstance()
//            c.timeInMillis = it.toLong() * 1000
//            val year = c.get(Calendar.YEAR)
//            val month = c.get(Calendar.MONTH)
//            val day = c.get(Calendar.DATE)
//            val monthText =
//                if (languageType == ChangeLanguageType.ENG.value) view.resources.getStringArray(
//                    R.array.monthListEN
//                )[month] else view.resources.getStringArray(R.array.monthListTH)[month]
//            view.text =
//                "${String.format(
//                    "%02d",
//                    day
//                )} $monthText ${if (languageType == ChangeLanguageType.ENG.value) year else year + 543}"
//        }
//    }
//
//    @SuppressLint("SetTextI18n", "SimpleDateFormat")
//    @JvmStatic
//    @BindingAdapter(value = ["shortMonthTimeStampToDateTime", "languageType"], requireAll = true)
//    fun setShortMonthTimeStampToDateTime(
//        view: AppCompatTextView,
//        timeStamp: String?,
//        languageType: String?
//    ) {
//        timeStamp?.let {
//            if (it.isNotEmpty()) {
//                val c = Calendar.getInstance()
//                c.timeInMillis = it.toLong() * 1000
//                val year = c.get(Calendar.YEAR)
//                val month = c.get(Calendar.MONTH)
//                val day = c.get(Calendar.DATE)
//                val monthText =
//                    if (languageType == ChangeLanguageType.ENG.value) view.resources.getStringArray(
//                        R.array.monthShortListEN
//                    )[month] else view.resources.getStringArray(R.array.monthShortListTH)[month]
//                view.text =
//                    "${String.format(
//                        "%02d",
//                        day
//                    )} $monthText ${if (languageType == ChangeLanguageType.ENG.value) year else (year + 543)}  ${String.format(
//                        "%02d:%02d",
//                        c.get(Calendar.HOUR_OF_DAY),
//                        c.get(Calendar.MINUTE)
//                    )}"
//            }
//        }
//    }
//
//    @SuppressLint("SetTextI18n", "SimpleDateFormat")
//    @JvmStatic
//    @BindingAdapter("timeStampToTime")
//    fun setTimeStampToTime(
//        view: AppCompatTextView,
//        timeStamp: String?
//    ) {
//        timeStamp?.let {
//            if (it.isNotEmpty()) {
//                val c = Calendar.getInstance()
//                c.timeInMillis = it.toLong() * 1000
//                view.text =
//                    " ${String.format(
//                        "%02d:%02d",
//                        c.get(Calendar.HOUR_OF_DAY),
//                        c.get(Calendar.MINUTE)
//                    )}"
//            }
//        }
//    }
//
//    @SuppressLint("SetTextI18n", "SimpleDateFormat")
//    @JvmStatic
//    @BindingAdapter(value = ["shortMonthStampToDateTime", "languageType"], requireAll = true)
//    fun setShortMonthStampToDateTime(
//        view: AppCompatTextView,
//        timeStamp: String?,
//        languageType: String?
//    ) {
//        timeStamp?.let {
//            if (it.isNotEmpty()) {
//                val c = Calendar.getInstance()
//                c.timeInMillis = it.toLong() * 1000
//                val year = c.get(Calendar.YEAR)
//                val month = c.get(Calendar.MONTH)
//                val day = c.get(Calendar.DATE)
//                val monthText =
//                    if (languageType == ChangeLanguageType.ENG.value) view.resources.getStringArray(
//                        R.array.monthShortListEN
//                    )[month] else view.resources.getStringArray(R.array.monthShortListTH)[month]
//                view.text =
//                    "${String.format(
//                        "%02d",
//                        day
//                    )} $monthText ${if (languageType == ChangeLanguageType.ENG.value) year else (year + 543)}"
//            }
//        }
//    }
//
//    @SuppressLint("SetTextI18n", "SimpleDateFormat")
//    @JvmStatic
//    @BindingAdapter(value = ["dateCodeToDateDisplay", "languageType"], requireAll = true)
//    fun setDateCodeToDateDisplay(
//        view: TextView,
//        dateCode: String?,
//        languageType: String?
//    ) {
//        when {
//            dateCode.isNullOrEmpty() -> {
//                view.text = "-"
//                return
//            }
//            else -> dateCode.let {
//                val df = SimpleDateFormat("yyyy-MM-dd")
//                val year: Int
//                val month: Int
//                val day: Int
//                try {
//                    val date = df.parse(dateCode)
//                    val c = Calendar.getInstance()
//                    c.time = date!!
//                    year = c.get(Calendar.YEAR)
//                    month = c.get(Calendar.MONTH)
//                    day = c.get(Calendar.DATE)
//                    val monthText =
//                        if (languageType == ChangeLanguageType.ENG.value) view.resources.getStringArray(
//                            R.array.monthListEN
//                        )[month] else view.resources.getStringArray(R.array.monthListTH)[month]
//                    view.text =
//                        "${String.format(
//                            "%02d",
//                            day
//                        )} $monthText ${if (languageType == ChangeLanguageType.ENG.value) year else year + 543}"
//                } catch (e: ParseException) {
//                    e.printStackTrace()
//                }
//            }
//        }
//    }
//
//    @SuppressLint("SetTextI18n", "SimpleDateFormat")
//    @JvmStatic
//    @BindingAdapter(value = ["dateCodeAndTimeToDateDisplay", "languageType"], requireAll = true)
//    fun setDateCodeAndTimeToDateDisplay(
//        view: AppCompatTextView,
//        dateCode: String?,
//        languageType: String?
//    ) {
//        dateCode?.let {
//            if (it.isEmpty()) return
//            val df = SimpleDateFormat("yyyy-MM-dd h:m:s")
//            val year: Int
//            val month: Int
//            val day: Int
//            try {
//                val date = df.parse(dateCode)
//                val c = Calendar.getInstance()
//                c.time = date!!
//                year = c.get(Calendar.YEAR)
//                month = c.get(Calendar.MONTH)
//                day = c.get(Calendar.DATE)
//                val monthText =
//                    if (languageType == ChangeLanguageType.ENG.value) view.resources.getStringArray(
//                        R.array.monthListEN
//                    )[month] else view.resources.getStringArray(R.array.monthListTH)[month]
//                view.text =
//                    "${String.format(
//                        "%02d",
//                        day
//                    )} $monthText ${if (languageType == ChangeLanguageType.ENG.value) year else year + 543}"
//            } catch (e: ParseException) {
//                e.printStackTrace()
//            }
//        }
//    }
//
//    @SuppressLint("SetTextI18n", "SimpleDateFormat")
//    @JvmStatic
//    @BindingAdapter("dateCodeAndTimeToTimeDisplay")
//    fun setDateCodeAndTimeToTimeDisplay(
//        view: AppCompatTextView,
//        dateCode: String?
//    ) {
//        dateCode?.let {
//            val df = SimpleDateFormat("yyyy-MM-dd H:m:s")
//            val hours: Int
//            val minutes: Int
//            try {
//                val date = df.parse(dateCode)
//                val c = Calendar.getInstance()
//                c.time = date!!
//                hours = c.get(Calendar.HOUR_OF_DAY)
//                minutes = c.get(Calendar.MINUTE)
//                view.text = "$hours:$minutes"
//            } catch (e: ParseException) {
//                e.printStackTrace()
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter("refreshing")
//    fun SwipeRefreshLayout.refreshing(visible: Boolean) {
//        isRefreshing = visible
//    }
//
//    @SuppressLint("SetTextI18n")
//    @JvmStatic
//    @BindingAdapter(value = ["phoneSecure", "strRes"], requireAll = false)
//    fun AppCompatTextView.setPhoneSecure(phoneText: String?, strRes: String?) {
//        if (phoneText != null && strRes != null) {
//            val mockSecure = "XXX XXX "
//            text = "$strRes$mockSecure${phoneText.takeLast(4)}"
//        }
//    }
//
//    @SuppressLint("SetTextI18n")
//    @JvmStatic
//    @BindingAdapter(value = ["emailSecure", "strRes"], requireAll = false)
//    fun AppCompatTextView.setEmailSecure(emailText: String?, strRes: String?) {
//        if (!emailText.isNullOrEmpty() && !strRes.isNullOrEmpty()) {
//            if (emailText.isInt()) {
//                val mockSecure = "XXX XXX"
//                text = "$strRes$mockSecure${emailText.takeLast(4)}"
//            } else {
//                text = "$strRes${emailText.replaceRange(2, emailText.indexOf("@"), "****")}"
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter("bmiStatusBackground")
//    fun setBMIStatusBackground(view: View, bmi: String?) {
//        bmi?.let {
//            val newBMI = bmi.toFloat()
//            when {
//                newBMI < 18.5 -> view.setBackgroundResource(R.drawable.bg_status_bmi_low_level_gradient)
//                newBMI < 22.9 -> view.setBackgroundResource(R.drawable.bg_status_bmi_normal_level_gradient)
//                newBMI < 24.9 -> view.setBackgroundResource(R.drawable.bg_status_bmi_hight_level_gradient)
//                newBMI < 29.9 -> view.setBackgroundResource(R.drawable.bg_status_bmi_hight_level_gradient)
//                else -> view.setBackgroundResource(R.drawable.bg_status_bmi_over_level_gradient)
//            }
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter("qrGenerate")
//    fun setQRGenerate(view: AppCompatImageView, text: String?) {
//        text?.let {
//            val bitmap = QRCode.from(text).withSize(500, 500).bitmap()
//            view.setImageBitmap(bitmap)
//        }
//    }
//
//    @JvmStatic
//    @BindingAdapter("goneUnless")
//    fun setGoneUnless(view: View, visible: Boolean) {
//        view.visibility = if (visible) View.VISIBLE else View.GONE
//    }
//
//    // @SuppressLint("SetTextI18n")
//// @JvmStatic
//// @BindingAdapter(value = ["currencyMoney", "currencyText"], requireAll = false)
//// fun setCurrencyMoney(view: TextView, money: String?, text: String? = "") {
////     if (TextUtils.isDigitsOnly(money ?: "")) {
////         try {
////             val symbols = DecimalFormatSymbols()
////             symbols.decimalSeparator = ','
////             val decimalFormat = DecimalFormat("###,###,###,###", symbols)
////             view.text = "${decimalFormat.format(money?.toDouble())}${if (text.isNullOrEmpty()) "" else text}"
////         } catch (e: Exception) {
////             d { "setCurrencyMoney Exception ${e.message}" }
////         }
////     } else {
////         view.text = "$money${if (text.isNullOrEmpty()) "" else text}"
////     }
////
//// }
//    fun getStringToUri(context: Context, uri: String?): Bitmap {
//        return if (!uri.isNullOrEmpty())
//            return try {
//                val options = BitmapFactory.Options()
//                options.inSampleSize = 2
//                BitmapFactory.decodeFile(uri, options)
//            } catch (e: Exception) {
//                BitmapHelper.drawableToBitmap(
//                    ContextCompat.getDrawable(
//                        context,
//                        R.drawable.ic_placeholder
//                    )!!
//                )
//            }
//        else BitmapHelper.drawableToBitmap(
//            ContextCompat.getDrawable(
//                context,
//                R.drawable.ic_placeholder
//            )!!
//        )
//    }
}
