package com.app.car.di.module

import android.content.Context
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class OkHttpBuilder(
    private val loggingInterceptor: HttpLoggingInterceptor,
    private val chuckInterceptor: ChuckInterceptor,
    private val defaultRequestInterceptor: DefaultRequestInterceptor,
    private val context: Context
) {

    fun build(): OkHttpClient {
        return with(OkHttpClient.Builder()) {
            addInterceptor(loggingInterceptor)
            addInterceptor(chuckInterceptor)
            addInterceptor(defaultRequestInterceptor)
            addInterceptor(NetworkConnectionInterceptor(context))
//            addInterceptor(TokenExpiredInterceptor)
            connectTimeout(60, TimeUnit.SECONDS)
            readTimeout(30, TimeUnit.SECONDS)
            writeTimeout(30, TimeUnit.SECONDS)
            build()
        }
    }
}
