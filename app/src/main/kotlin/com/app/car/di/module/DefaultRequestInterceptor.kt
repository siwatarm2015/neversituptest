package com.app.car.di.module

import android.annotation.SuppressLint
import android.os.Build
import okhttp3.Interceptor
import okhttp3.Response

class DefaultRequestInterceptor : Interceptor {

    @SuppressLint("HardwareIds")
    override fun intercept(chain: Interceptor.Chain): Response {

        return chain.proceed(
            with(
                chain.request().newBuilder()
            ) {
//                addHeader("Accept-Language", Hawk.get(LANGUAGE))
                addHeader("Os", "android")
                addHeader("Model", Build.MODEL)
                build()
            }
        )
    }
}
