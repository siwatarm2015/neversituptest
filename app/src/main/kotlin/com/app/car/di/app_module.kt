package com.app.car.di

import android.annotation.SuppressLint
import android.app.Application
import android.provider.Settings
import androidx.room.Room
import com.app.car.BuildConfig
import com.app.car.data.datasource.local.db.DatabaseManager
import com.app.car.data.datasource.remote.RemoteDataSource
import com.app.car.data.repository.MainRepository
import com.app.car.data.repository.NetworkStatusRepository
import com.app.car.data.service.ApiService
import com.app.car.di.module.DefaultRequestInterceptor
import com.app.car.di.module.OkHttpBuilder
import com.app.car.di.module.RetrofitBuilder
import com.app.car.domain.dao.UserDao
import com.app.car.domain.usecase.home.GetCustomersUseCase
import com.app.car.domain.usecase.login.LoginUseCase
import com.app.car.presentation.NetworkStatusViewModel
import com.app.car.presentation.ui.MainViewModel
import com.app.car.presentation.ui.detail.CustomerViewModel
import com.app.car.presentation.ui.home.HomeViewModel
import com.app.car.presentation.ui.login.LoginViewModel
import com.google.gson.GsonBuilder
import com.orhanobut.logger.Logger.i
import com.orhanobut.logger.Logger.json
import com.readystatesoftware.chuck.ChuckInterceptor
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory

val appModule: Module = module {
    single { GsonBuilder().serializeNulls().create()!! }
}

//val activityMainScopeModule = module {
//    scope(named<MainActivity>()) {
//        scoped {
//            ProvideBaseActivityListener()
//        }
//    }
//}

@SuppressLint("HardwareIds")
val networkModule = module {

    single {
        OkHttpBuilder(
            get(),
            get(),
            get(),
            androidContext()
        ).build()
    }

    single<String>(named("uniqueID")) {
        Settings.Secure.getString(
            androidContext().contentResolver,
            Settings.Secure.ANDROID_ID
        )
    }


    single<Converter.Factory> { GsonConverterFactory.create() }

    single { DefaultRequestInterceptor() }

    single<ChuckInterceptor> { ChuckInterceptor(get()).showNotification(true) }

    single {

        HttpLoggingInterceptor { message ->
            try {
                JSONObject(message)
                json(message)
            } catch (error: JSONException) {
                i(message)
            }
        }.apply { level = HttpLoggingInterceptor.Level.BODY }

    }

    single<ApiService>(named("retrofit")) {
        RetrofitBuilder(
            get(),
            get()
        ).build(BuildConfig.BASE_URL)
    }

}

val viewModuleActivity = module {
    viewModel { MainViewModel(get()) }
}

val viewModuleFragment = module {
    viewModel { HomeViewModel(get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { CustomerViewModel(get()) }
    viewModel { NetworkStatusViewModel(get())}
}

val database = module {

    fun provideDatabase(application: Application): DatabaseManager {
        return Room.databaseBuilder(application, DatabaseManager::class.java, "user_database")
            .fallbackToDestructiveMigration()
            .build()
    }

    fun provideCountriesDao(database: DatabaseManager): UserDao {
        return database.userDao
    }

    single { provideDatabase(androidApplication()) }
    single { provideCountriesDao(get()) }

}

val useCase = module {
    single { GetCustomersUseCase(get()) }
    single { LoginUseCase(get()) }
}

val dataSourceModule = module {
    single {
        RemoteDataSource(
            apiService = get(named("retrofit")),
            userDao = get()
        )
    }

//    single {
//        LocalDataSource(
//            databaseManager = get()
//        )
//    }

}

val repositoryModule = module {

    fun provideAppScope(): CoroutineScope {
        return CoroutineScope(SupervisorJob() + Dispatchers.Main)
    }

    fun getCoroutineDispatcher(): CoroutineDispatcher {
        return Dispatchers.Main
    }

    single { MainRepository(get()) }
    single { NetworkStatusRepository(androidContext(), getCoroutineDispatcher(),provideAppScope()) }
}

val module =
    listOf(
        appModule,
        networkModule,
        useCase,
        database,
        dataSourceModule,
        repositoryModule,
        viewModuleActivity,
        viewModuleFragment
    )
