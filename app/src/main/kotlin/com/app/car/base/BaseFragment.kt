package com.app.car.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.car.R
import com.google.android.material.snackbar.Snackbar
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration

abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel<*>> : Fragment(),
    BaseActivity.BaseActivityListener, BaseActivity.BackPressedListener {

    protected open  var binding: T? = null
    protected abstract fun getViewModel(): V
    @LayoutRes
    abstract fun getLayoutId(): Int
    protected abstract fun getBindingVariable(): Int
    protected open val navController by lazy {
        findNavController()
    }

    protected abstract fun updateInstant(savedInstanceState: Bundle?)
    protected abstract fun updateUI(savedInstanceState: Bundle?)
    protected abstract fun updateListener()
    protected abstract fun observerLiveData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        updateInstant(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layoutId = getLayoutId()
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        binding?.lifecycleOwner = this
        binding?.setVariable(getBindingVariable(), getViewModel())
        binding?.executePendingBindings()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerLiveData()
        updateListener()
        updateUI(savedInstanceState)
    }

    inline fun <reified A : NavArgs> args(): A {
        return navArgs<A>().value
    }

    protected fun onBackPress(callback: () -> Unit) {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            callback.invoke()
        }
    }

    protected fun toast(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }

    protected fun showSnackBar(rootView: View, msg: String, callback: () -> Unit) {
        Snackbar.make(rootView, msg, Snackbar.LENGTH_SHORT).setAction(getString(R.string.ok)) {
            callback()
        }.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        this.binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*,*>) {
            Log.d("onAttach",">>>> setBackPressedListener")
            context.setBackPressedListener(this)
        } else {
            throw RuntimeException(context.toString() )
        }
    }

    fun <VH : RecyclerView.ViewHolder> setUpRcv(
        rcv: RecyclerView,
        adapter:
        RecyclerView.Adapter<VH>,
        isNestedScrollingEnabled: Boolean = false,
        orientation: Int = RecyclerView.VERTICAL,
        colorDivider: Int?,
        showLastDivider: Boolean
    ) {
        rcv.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = orientation
        rcv.layoutManager = layoutManager
        rcv.isNestedScrollingEnabled = isNestedScrollingEnabled
        if (colorDivider != null) {
            if (orientation == LinearLayoutManager.VERTICAL) {
                val config = HorizontalDividerItemDecoration
                    .Builder(context)
                    .color(ContextCompat.getColor(requireContext(), colorDivider)).apply {
                        if (showLastDivider) this.showLastDivider()
                    }.run {
                        this.build()
                    }
                rcv.addItemDecoration(config)
            }
        }
        rcv.adapter = adapter
    }

    override fun onKeyboardVisibility(isVisibility: Boolean) {}
    override fun provideChangeLanguage() {}
    override fun onBackPressActivityListener() {}

}