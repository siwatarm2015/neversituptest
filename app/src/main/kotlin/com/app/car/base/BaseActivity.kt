package com.app.car.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.NavHostFragment
import com.app.car.R
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel<*>> : AppCompatActivity() {
    lateinit var binding: T

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    @LayoutRes
    open fun getLayoutIdLoading(): Int = -1
    open fun getThemResId(): Int = -1
    protected abstract fun updateUI(savedInstanceState: Bundle?)
    protected abstract fun updateListener(savedInstanceState: Bundle?)
    protected abstract fun getViewModel(): V
    protected abstract fun getBindingVariable(): Int

    protected open val navController by lazy {
        supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
    }
    private var backPressedListener: BackPressedListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        //        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        performDataBinding()
        updateListener(savedInstanceState)
        updateUI(savedInstanceState)
    }

    private fun performDataBinding() {
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        binding.executePendingBindings()
        binding.setVariable(getBindingVariable(), getViewModel())
    }

    fun setBackPressedListener(backPressedListener: BackPressedListener?) {
        this.backPressedListener = backPressedListener
    }
    fun getBackPressedListener() = this.backPressedListener

    fun toast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    inline fun <reified T> LiveData<T>.observe(crossinline onNext: (T) -> Unit) {
        observe(this@BaseActivity, { onNext(it) })
    }

    inline fun <reified T : Activity> Activity.startActivity() {
        startActivity(Intent(this, T::class.java))
    }

    inline fun <reified T : Activity> Activity.startActivityWithIntent(bundle: Bundle) {
        val intent = Intent(this, T::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left)
    }

    inline fun <reified T : Activity> Activity.startActivityWithAnimation() {
        startActivity(Intent(this, T::class.java))
        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left)
    }

    inline fun <reified T : Activity> Activity.startActivityWithAnimationClearFlag() {
        startActivity(
            Intent(
                this,
                T::class.java
            ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left)
    }

    inline fun <reified T : Activity> AppCompatActivity.startActivityBundleWithAnimation(bundle: Bundle) {
        val intent = Intent(this, T::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left)
    }

    fun requireMultiPermission(permission: Collection<String>, callback: (grant: Boolean) -> Unit) {
        Dexter.withContext(this)
            .withPermissions(permission)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    callback(report.areAllPermissionsGranted())
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            })
            .withErrorListener {
                callback(false)
            }.check()
    }

    fun requirePermission(permission: String, callback: (grant: Boolean) -> Unit) {
        Dexter.withContext(this)
            .withPermission(permission)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(permission: PermissionGrantedResponse?) {
                    callback(true)
                }

                override fun onPermissionDenied(permission: PermissionDeniedResponse?) {
                    callback(false)
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            })
            .withErrorListener {
                callback(false)
            }.check()
    }


    interface BaseActivityListener {
        fun onKeyboardVisibility(isVisibility: Boolean)
        fun provideChangeLanguage()
    }

    interface BackPressedListener {
        fun onBackPressActivityListener()
    }

}
