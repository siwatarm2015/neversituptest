package com.app.car.base

interface BaseNavigator {

    fun clearKeyboard()

    fun goBackToPrevious()
}
