package com.app.car.base

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import java.lang.ref.WeakReference
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel<N> : ViewModel(), CoroutineScope {

    private lateinit var navigator: WeakReference<N>
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main + SupervisorJob()

    fun setNavigator(navigator: N) {
        this.navigator = WeakReference(navigator)
    }

    fun getNavigator(): N = navigator.get()!!

    @CallSuper
    override fun onCleared() {
        job.cancel()
        coroutineContext.cancel()
        super.onCleared()
    }
}
