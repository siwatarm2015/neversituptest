package com.app.car.presentation.ui.login

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.transition.TransitionInflater
import com.app.car.BR
import com.app.car.R
import com.app.car.base.BaseFragment
import com.app.car.databinding.FragmentLoginBinding
import com.app.car.domain.Result.*
import com.app.car.domain.model.request.LoginRequest
import com.app.car.extensions.setOnAnimateClickListener
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment :
    BaseFragment<FragmentLoginBinding, LoginViewModel>(),
    LoginNavigator {

    private val viewModels: LoginViewModel by viewModel()

    override fun getViewModel(): LoginViewModel = viewModels

    override fun getLayoutId(): Int = R.layout.fragment_login

    override fun getBindingVariable(): Int = BR.model

    override fun updateInstant(savedInstanceState: Bundle?) {
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun observerLiveData() {
        viewModels.loginResponse.observe(viewLifecycleOwner, { result ->
            when (result) {
                is Success -> {
                    result.data?.let {
                        navController.navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
                    }
                }
                is Failure -> {
                }
                is Loading -> {
                }
            }
        })
    }

    override fun updateListener() {

        binding?.btnLogin?.setOnAnimateClickListener {
            viewModels.login(
                loginRequest = LoginRequest(
                    username = binding?.edtUserName?.text.toString(),
                    password = binding?.edtPassword?.text.toString()
                )
            )
        }
    }

    override fun updateUI(savedInstanceState: Bundle?) {
        viewModels.setNavigator(this)
        binding?.viewModel = viewModels
        initRecyclerView()

    }

    private fun initRecyclerView() {

    }

    override fun navigateToDashBoard(id: String) {

    }

    override fun clearKeyboard() {
    }

    override fun onBackPressActivityListener() {
        requireActivity().finish()
    }

    override fun goBackToPrevious() {
        navController.popBackStack()
    }

}
