package com.app.car.presentation.ui.template

import com.app.car.base.BaseViewModel
import com.app.car.domain.BaseResponse
import com.app.car.domain.Result
import com.app.car.domain.Result.Loading
import com.app.car.domain.usecase.home.GetCustomersUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class TemplateViewModel(
    private val getCustomersUseCase: GetCustomersUseCase
) : BaseViewModel<TemplateNavigator>() {


    private val _mainResponse =
        MutableStateFlow<Result<BaseResponse<Nothing>>>(Loading(isLoad = true))
    val mainResponse: StateFlow<Result<BaseResponse<Nothing>>> = _mainResponse

//    fun getHome(getHomeRequest: GetHomeRequest) {
//        viewModelScope.launch {
//            try {
//                retryIO(times = 3) {
//                    getUserUseCase().onStart {
//                        _mainResponse.value = Loading(true)
//                    }.collect {
////                        _mainResponse.value = it
//                    }
//                }
//            } catch (e: NoConnectivityException) {
//                _mainResponse.value = Failure(e.message)
//            } catch (e: Exception) {
//                _mainResponse.value = Failure(e.message)
//            }
//        }
//    }
}
