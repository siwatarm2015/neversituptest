package com.app.car.presentation.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.app.car.base.BaseViewModel
import com.app.car.domain.Result
import com.app.car.domain.Result.Loading
import com.app.car.domain.model.login.Customers
import com.app.car.domain.model.users.UsersResponse
import com.app.car.domain.usecase.home.GetCustomersUseCase
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(
    private val getCustomersUseCase: GetCustomersUseCase
) : BaseViewModel<HomeNavigator>() {

    private val _usersResponse =
        MutableStateFlow<Result<MutableList<UsersResponse>>>(Loading(isLoad = true))
    val usersResponse: StateFlow<Result<MutableList<UsersResponse>>> = _usersResponse

    private val _localResponse =
        LiveEvent<MutableList<Customers>>()
    val localResponse: LiveData<MutableList<Customers>> = _localResponse

    private val _userToken = LiveEvent<String>()
    val userToken: LiveData<String> = _userToken

    private val _userLogout = LiveEvent<Boolean>()
    val userLogout: LiveData<Boolean> = _userLogout

    fun getUserToken() {
        viewModelScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    getCustomersUseCase.getUserToken()
                }.apply {
                    _userToken.value = this
                }
            } catch (e: Exception) {

            }
        }
    }

    fun getCustomersLocal() {
        viewModelScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    getCustomersUseCase()
                }.apply {
                    _localResponse.value = this
                }
            } catch (e: Exception) {

            }
        }
    }

    fun logout() {
        viewModelScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    getCustomersUseCase.logout()
                }.apply {
                    _userLogout.value = this
                }
            } catch (e: Exception) {

            }
        }
    }
}
