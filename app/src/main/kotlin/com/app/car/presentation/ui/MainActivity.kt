package com.app.car.presentation.ui

import android.os.Bundle
import com.app.car.BR
import com.app.car.R
import com.app.car.base.BaseActivity
import com.app.car.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(),
    MainNavigator {

    private val viewModels: MainViewModel by viewModel()

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun getBindingVariable(): Int = BR.model

    override fun getViewModel(): MainViewModel = viewModels


    override fun updateListener(savedInstanceState: Bundle?) {

    }

    override fun updateUI(savedInstanceState: Bundle?) {

    }

    override fun navigateToNotificationFragment() {

    }

    override fun clearKeyboard() {

    }

    override fun goBackToPrevious() {

    }

    override fun onBackPressed() {
        getBackPressedListener()?.onBackPressActivityListener()
//        when (navController?.id) {
//            R.id.homeFragment -> {
////                 super.onBackPressed()
//            }
//            else -> {
//                getBackPressedListener()?.onBackPressActivityListener()
//            }
//        }
    }

}