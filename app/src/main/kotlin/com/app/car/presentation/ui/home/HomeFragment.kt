package com.app.car.presentation.ui.home

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.app.car.BR
import com.app.car.R
import com.app.car.base.BaseFragment
import com.app.car.common.bundle.CustomerFragmentBundle
import com.app.car.common.bundle.UserFragmentBundle
import com.app.car.databinding.FragmentHomeBinding
import com.app.car.domain.NetworkStatusState
import com.app.car.domain.NetworkStatusState.*
import com.app.car.domain.Result.*
import com.app.car.domain.model.TemplateModel
import com.app.car.domain.model.login.Customers
import com.app.car.domain.model.users.UsersResponse
import com.app.car.extensions.gone
import com.app.car.extensions.setOnAnimateClickListener
import com.app.car.extensions.visible
import com.app.car.presentation.NetworkStatusViewModel
import com.app.car.presentation.ui.home.adapter.UsersAdapter
import com.orhanobut.hawk.Hawk
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class HomeFragment :
    BaseFragment<FragmentHomeBinding, HomeViewModel>(),
    HomeNavigator, UsersAdapter.OnItemClickListener {

    private val viewModels: HomeViewModel by viewModel()

    private val networkViewModel: NetworkStatusViewModel by viewModel()

    override fun getViewModel(): HomeViewModel = viewModels

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun getBindingVariable(): Int = BR.model

    private lateinit var usersAdapter: UsersAdapter

    private var customersListResponse = mutableListOf<Customers>()

    private var token = ""

    private var customerId = ""

    override fun updateInstant(savedInstanceState: Bundle?) {
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun observerLiveData() {

        viewModels.userLogout.observe(viewLifecycleOwner, {
            when (it) {
                true -> navigateToLogin()
                false -> {
                    Timber.d("fail")
                }
            }
        })

        viewModels.localResponse.observe(viewLifecycleOwner, { result ->
            result?.let {
                customersListResponse = it
                setUsersListData(data = it)
            }
        })

        viewModels.userToken.observe(viewLifecycleOwner, {
            token = it
            navigateToDetail(token)
        })
    }

    override fun updateListener() {
        binding?.btnLogout?.setOnAnimateClickListener {
            navController.popBackStack()
//            viewModels.logout()
        }
    }

    override fun updateUI(savedInstanceState: Bundle?) {
        viewModels.setNavigator(this)
        binding?.viewModel = viewModels
        initRecyclerView()
        viewModels.getCustomersLocal()
    }

    private fun initRecyclerView() {
        usersAdapter = UsersAdapter()
        binding?.recyclerView?.apply {
            setUpRcv(
                rcv = this,
                adapter = usersAdapter,
                orientation = RecyclerView.VERTICAL,
                colorDivider = R.color.colorBlack,
                showLastDivider = true
            )
        }.run {
            usersAdapter.callback = this@HomeFragment
        }
    }

    private fun setUsersListData(data: MutableList<Customers>) {
        usersAdapter.customersListResponse = data
        usersAdapter.notifyDataSetChanged()
    }

    override fun navigateToDetail(id: String) {
        navController.navigate(
            HomeFragmentDirections.actionHomeFragmentToUserDetailFragment(
                customerBundle = CustomerFragmentBundle(
                    token = token,
                    customerId = customerId
                )
            )
        )
    }

    override fun clearKeyboard() {
    }

    override fun onBackPressActivityListener() {
        requireActivity().finish()
    }

    override fun navigateToLogin() {
        navController.navigate(HomeFragmentDirections.actionLoginFragmentToHomeFragment())
    }

    override fun goBackToPrevious() {
        navController.popBackStack()
    }

    override fun onItemClick(position: Int) {
        customerId = customersListResponse[position].userId
        viewModels.getUserToken()
    }

}
