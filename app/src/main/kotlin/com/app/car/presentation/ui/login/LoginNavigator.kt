package com.app.car.presentation.ui.login

import com.app.car.base.BaseNavigator

interface LoginNavigator : BaseNavigator {

    fun navigateToDashBoard(id: String)
}
