package com.app.car.presentation.ui.home

import com.app.car.base.BaseNavigator

interface HomeNavigator : BaseNavigator {

    fun navigateToDetail(id: String)

    fun navigateToLogin()
}
