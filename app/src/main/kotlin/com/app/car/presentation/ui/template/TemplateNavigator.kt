package com.app.car.presentation.ui.template

import com.app.car.base.BaseNavigator

interface TemplateNavigator : BaseNavigator {

    fun navigateToRegisterIDCardFragment()
}
