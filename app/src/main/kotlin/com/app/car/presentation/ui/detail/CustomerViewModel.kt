package com.app.car.presentation.ui.detail

import androidx.lifecycle.viewModelScope
import com.app.car.base.BaseViewModel
import com.app.car.domain.Result
import com.app.car.domain.Result.Failure
import com.app.car.domain.Result.Loading
import com.app.car.domain.model.login.CustomerDetailResponse
import com.app.car.domain.model.request.CustomerDetailRequest
import com.app.car.domain.model.users.UsersResponse
import com.app.car.domain.usecase.home.GetCustomersUseCase
import com.app.car.exception.NoConnectivityException
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class CustomerViewModel(
    private val getCustomersUseCase: GetCustomersUseCase
) : BaseViewModel<UserNavigator>() {

    private val _customerDetailResponse =
        MutableStateFlow<Result<CustomerDetailResponse>>(Loading(isLoad = true))
    val customerDetailResponse: StateFlow<Result<CustomerDetailResponse>> = _customerDetailResponse

    fun getCustomerDetail(customerDetailRequest: CustomerDetailRequest) {
        viewModelScope.launch {
            try {
                getCustomersUseCase(customerDetailRequest).onStart {
                    _customerDetailResponse.value = Loading(true)
                }.collect {
                    _customerDetailResponse.value = it
                }
            } catch (e: NoConnectivityException) {
                _customerDetailResponse.value = Failure(e.message)
            } catch (e: Exception) {
                _customerDetailResponse.value = Failure(e.message)
            }
        }
    }
}
