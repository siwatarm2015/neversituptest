package com.app.car.presentation

import androidx.annotation.MainThread
import androidx.lifecycle.ViewModel
import com.app.car.data.repository.NetworkStatusRepository
import com.app.car.domain.NetworkStatusState
import kotlinx.coroutines.flow.StateFlow

@MainThread
class NetworkStatusViewModel(
    private val repo: NetworkStatusRepository
) : ViewModel() {

    /** [StateFlow] emitting a [NetworkStatusState] every time it changes */
    val networkState: StateFlow<NetworkStatusState> = repo.state

    /* Simple helper/getter for fetching network connection status synchronously */
    fun isDeviceOnline() : Boolean = repo.hasNetworkConnection()

}