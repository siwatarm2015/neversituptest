package com.app.car.presentation.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.car.R
import com.app.car.base.BaseViewHolder
import com.app.car.databinding.ItemUserBinding
import com.app.car.domain.model.login.Customers
import com.app.car.domain.model.users.UsersResponse

class UsersAdapter : RecyclerView.Adapter<BaseViewHolder<*>>() {

//    var items: List<MapDataEvenModel> by Delegates.observable(emptyList()) { prop, old, new ->
//        autoNotify(
//            old,
//            new
//        ) { o, n ->
//            n.isTypeYear == o.isTypeYear && n.event.id == o.event.id
//        }
//    }
    lateinit var callback: OnItemClickListener
    var customersListResponse: MutableList<Customers>? = null

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemUserBinding =
            DataBindingUtil.inflate(inflater, R.layout.item_user, parent, false)
        return UsersAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        return when (holder) {
            is UsersAdapterViewHolder -> {
                holder.bind(position)
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
    }

    override fun getItemCount(): Int = customersListResponse?.size ?: 0

    inner class UsersAdapterViewHolder(binding: ItemUserBinding) :
        BaseViewHolder<ItemUserBinding>(binding = binding) {
        fun bind(position: Int) {
            binding.adapter = this
            binding.position = position
            binding.model = customersListResponse?.get(position)
            binding.executePendingBindings()
        }

        fun onItemClick(position: Int){
            callback.onItemClick(position)
        }

    }
}