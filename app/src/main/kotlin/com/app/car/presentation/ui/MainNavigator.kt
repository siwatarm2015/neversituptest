package com.app.car.presentation.ui

import com.app.car.base.BaseNavigator

interface MainNavigator : BaseNavigator {
    fun navigateToNotificationFragment()
}
