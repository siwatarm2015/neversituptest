package com.app.car.presentation.ui.template

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.transition.TransitionInflater
import com.app.car.BR
import com.app.car.R
import com.app.car.base.BaseFragment
import com.app.car.databinding.FragmentTemplateBinding
import com.app.car.domain.Result.*
import com.app.car.domain.model.TemplateModel
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel


class TemplateFragment :
    BaseFragment<FragmentTemplateBinding, TemplateViewModel>(),
    TemplateNavigator {

    private val viewModels: TemplateViewModel by viewModel()

    override fun getViewModel(): TemplateViewModel = viewModels

    override fun getLayoutId(): Int = R.layout.fragment_template

    override fun getBindingVariable(): Int = BR.model


    private var mArguments: TemplateModel? = null

    private var isInit = false

    override fun updateInstant(savedInstanceState: Bundle?) {
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        // Receiver argument
        // arguments?.let {
        //     credentialsDao = LoginFragmentArgs.fromBundle(it).model
        // }
    }

    override fun updateListener() {
//        btnBack.clickWithDebounce {
//            goBackToPrevious()
//        }
//        binding?.data = mArguments
    }

    override fun updateUI(savedInstanceState: Bundle?) {
        viewModels.setNavigator(this)
        binding?.viewModel = viewModels
    }

    override fun observerLiveData() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModels.mainResponse.collect { result ->
                when (result) {
                    is Success -> TODO()
                    is Failure -> TODO()
                    is Loading -> TODO()
                }
            }
        }
    }

    override fun navigateToRegisterIDCardFragment() {
        // navController.navigate(
        //     RegisterEmailFragmentDirections.actionRegisterEmailFragmentToRegisterIDCardFragment()
        // )
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun clearKeyboard() {
    }


    override fun onBackPressActivityListener() {
        navController.popBackStack()
    }

    override fun goBackToPrevious() {
        navController.popBackStack()
    }

}
