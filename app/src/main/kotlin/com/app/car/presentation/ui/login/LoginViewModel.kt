package com.app.car.presentation.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.app.car.base.BaseViewModel
import com.app.car.domain.Result
import com.app.car.domain.Result.Failure
import com.app.car.domain.Result.Loading
import com.app.car.domain.model.login.LoginResponse
import com.app.car.domain.model.request.LoginRequest
import com.app.car.domain.usecase.login.LoginUseCase
import com.app.car.exception.NoConnectivityException
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(
    private val loginUseCase: LoginUseCase
) : BaseViewModel<LoginNavigator>() {

    private val _loginResponse =
        LiveEvent<Result<LoginResponse>>()
    val loginResponse: LiveData<Result<LoginResponse>> = _loginResponse

    fun login(loginRequest: LoginRequest) {

        viewModelScope.launch {
            try {
                _loginResponse.value = Loading(true)
                withContext(Dispatchers.IO) {
                    loginUseCase.login(loginRequest)
                }.apply {
                    _loginResponse.value = this
                }
            } catch (e: NoConnectivityException) {
                _loginResponse.value = Failure(e.message)
            } catch (e: Exception) {
                _loginResponse.value = Failure(e.message)
            }
        }
    }

}
