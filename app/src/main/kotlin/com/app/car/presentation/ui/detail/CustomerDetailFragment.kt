package com.app.car.presentation.ui.detail

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.transition.TransitionInflater
import com.app.car.BR
import com.app.car.R
import com.app.car.base.BaseFragment
import com.app.car.common.bundle.CustomerFragmentBundle
import com.app.car.databinding.FragmentCustomerDetailBinding
import com.app.car.domain.Result.*
import com.app.car.domain.model.login.CustomerDetailResponse
import com.app.car.domain.model.request.CustomerDetailRequest
import com.app.car.presentation.ui.home.adapter.UsersAdapter
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class CustomerDetailFragment :
    BaseFragment<FragmentCustomerDetailBinding, CustomerViewModel>(),
    UserNavigator {

    private val viewModels: CustomerViewModel by viewModel()

    override fun getViewModel(): CustomerViewModel = viewModels

    override fun getLayoutId(): Int = R.layout.fragment_customer_detail

    override fun getBindingVariable(): Int = BR.model

    private var mArguments: CustomerFragmentBundle? = null

    private lateinit var usersAdapter: UsersAdapter

    private var token = ""

    private var customerId = ""

    override fun onResume() {
        super.onResume()
//        viewModels.getUsers()
    }

    override fun updateInstant(savedInstanceState: Bundle?) {
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        mArguments = args<CustomerDetailFragmentArgs>().customerBundle
    }

    override fun observerLiveData() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModels.customerDetailResponse.collect { result ->
                when (result) {
                    is Success -> {
                        updateView(result.data?.data)
                    }
                    is Failure -> {
                    }
                    is Loading -> {
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateView(data: CustomerDetailResponse.Data?) {
        binding?.txtId?.text = "ID : ${data?.id}"
        binding?.txtName?.text = "Name : ${data?.name}"
        binding?.txtSex?.text = "Sex : ${data?.sex}"
        binding?.txtCustomerGrade?.text = "CustomerGrade : ${data?.customerGrade}"
        binding?.txtIsCustomerPremium?.text = "IsCustomerPremium : ${data?.isCustomerPremium.toString()}"
    }

    override fun updateListener() {
//        btnBack.clickWithDebounce {
//            goBackToPrevious()
//        }
//        binding?.data = mArguments
    }

    override fun updateUI(savedInstanceState: Bundle?) {
        viewModels.setNavigator(this)
        binding?.viewModel = viewModels
        viewModels.getCustomerDetail(
            customerDetailRequest = CustomerDetailRequest(
                token = mArguments?.token ?:"",
                customerId = mArguments?.customerId ?:""
            )
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun clearKeyboard() {
    }

    override fun onBackPressActivityListener() {
        goBackToPrevious()
    }

    override fun goBackToPrevious() {
        navController.popBackStack()
    }

}
