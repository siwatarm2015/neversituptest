package com.app.car.data.service

import com.app.car.domain.BaseResponse
import com.app.car.domain.model.login.CustomerDetailResponse
import com.app.car.domain.model.login.LoginResponse
import com.app.car.domain.model.request.CustomerDetailRequest
import com.app.car.domain.model.request.GetHomeRequest
import com.app.car.domain.model.request.LoginRequest
import com.app.car.domain.model.users.UsersResponse
import retrofit2.Response
import retrofit2.http.*

/**
 * REST API access points
 */

interface ApiService {

    @GET("main")
    suspend fun getHome(
        @Header("Authorization") token: String,
        @Query("longitude") longitude: Double,
        @Query("latitude") latitude: Double,
        @Query("radius") radius: Int
    ): Response<BaseResponse<Nothing>>

    @GET("main")
    suspend fun getHome(
        @Header("Authorization") token: String,
        @Body getHomeRequest: GetHomeRequest
    ): Response<BaseResponse<Nothing>>

    @GET("users")
    suspend fun getUsers(): Response<MutableList<UsersResponse>>

    @POST("login")
    suspend fun login(
        @Body loginRequest: LoginRequest
    ): Response<LoginResponse>

    @POST("getCustomerDetail")
    suspend fun getCustomerDetail(
        @Body customerDetailRequest: CustomerDetailRequest
    ): Response<CustomerDetailResponse>

    @GET("users/{id}")
    suspend

    fun getUsersById(
        @Path("id") id: String
    ): Response<UsersResponse>

}
