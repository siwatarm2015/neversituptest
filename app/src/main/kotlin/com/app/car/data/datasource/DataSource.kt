package com.app.car.data.datasource

import com.app.car.domain.Result
import com.app.car.domain.model.login.CustomerDetailResponse
import com.app.car.domain.model.login.Customers
import com.app.car.domain.model.login.LoginResponse
import com.app.car.domain.model.request.CustomerDetailRequest
import com.app.car.domain.model.request.LoginRequest
import com.app.car.domain.model.users.UsersResponse
import kotlinx.coroutines.flow.Flow

interface DataSource {
    suspend fun login(loginRequest: LoginRequest): Result<LoginResponse>

    suspend fun logout(): Boolean

    suspend fun getCustomer(
    ): MutableList<Customers>

    suspend fun getCustomerDetail(
        customerDetailRequest: CustomerDetailRequest
    ): Flow<Result<CustomerDetailResponse>>

    suspend fun getUserToken(): String
}

