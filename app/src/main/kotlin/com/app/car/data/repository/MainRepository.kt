package com.app.car.data.repository

import com.app.car.data.datasource.remote.RemoteDataSource
import com.app.car.domain.Result
import com.app.car.domain.model.login.CustomerDetailResponse
import com.app.car.domain.model.login.Customers
import com.app.car.domain.model.login.LoginResponse
import com.app.car.domain.model.request.CustomerDetailRequest
import com.app.car.domain.model.request.LoginRequest
import kotlinx.coroutines.flow.Flow


class MainRepository(
    private val remoteDataSource: RemoteDataSource
) {

    suspend fun login(loginRequest: LoginRequest): Result<LoginResponse> {
        return remoteDataSource.login(loginRequest = loginRequest)
    }

    suspend fun logout(): Boolean{
        return remoteDataSource.logout()
    }

    suspend fun getCustomer(): MutableList<Customers> {
        return remoteDataSource.getCustomer()
    }

    suspend fun getCustomerDetail(customerDetailRequest: CustomerDetailRequest): Flow<Result<CustomerDetailResponse>>  {
        return remoteDataSource.getCustomerDetail(customerDetailRequest)
    }

    suspend fun getUserToken(): String {
        return remoteDataSource.getUserToken()
    }

}
