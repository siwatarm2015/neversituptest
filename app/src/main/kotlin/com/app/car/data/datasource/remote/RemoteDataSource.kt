package com.app.car.data.datasource.remote

import com.app.car.data.datasource.DataSource
import com.app.car.data.service.ApiService
import com.app.car.domain.Result
import com.app.car.domain.Result.Failure
import com.app.car.domain.Result.Success
import com.app.car.domain.dao.UserDao
import com.app.car.domain.model.login.CustomerDetailResponse
import com.app.car.domain.model.login.Customers
import com.app.car.domain.model.login.LoginResponse
import com.app.car.domain.model.request.CustomerDetailRequest
import com.app.car.domain.model.request.LoginRequest
import com.app.car.exception.NoConnectivityException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.retryWhen
import kotlinx.coroutines.withContext

class RemoteDataSource(
    private val apiService: ApiService,
    private val userDao: UserDao
) : DataSource {

    override suspend fun login(
        loginRequest: LoginRequest
    ): Result<LoginResponse> {
        val response = apiService.login(loginRequest = loginRequest)
        return try {
            if (response.isSuccessful) {
                response.body()?.let {
                    withContext(Dispatchers.IO) {
                        userDao.deleteUserTable()
                        userDao.add(users = it)
                        userDao.deleteCustomerTable()
                        userDao.addCustomer(customer = it.customers)
                    }
                }
                Success(response.body())
            } else {
                Failure(response.message())
            }
        } catch (throwable: NoConnectivityException) {
            Failure(throwable.message)
        }
    }

    override suspend fun logout(): Boolean {
        userDao.deleteCustomerTable()
        userDao.deleteUserTable()
        return true
    }

    override suspend fun getCustomer(): MutableList<Customers> {
        return userDao.getCustomer()
    }

    override suspend fun getCustomerDetail(customerDetailRequest: CustomerDetailRequest): Flow<Result<CustomerDetailResponse>> {
        return flow {
            val response = apiService.getCustomerDetail(
                customerDetailRequest = customerDetailRequest
            )
            try {
                emit(Success(response.body()))
            } catch (throwable: NoConnectivityException) {
                emit(Failure(throwable.message))
            }
        }.retryWhen { cause, attempt ->
            when {
                cause is NoConnectivityException && attempt < 3 -> {
                    delay(2000)
                    true
                }
                else -> {
                    false
                }
            }

        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getUserToken(): String {
        return userDao.getUserToken()
    }
}

