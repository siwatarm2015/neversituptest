package com.app.car.data.datasource.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.app.car.domain.dao.UserDao
import com.app.car.domain.model.login.Customers
import com.app.car.domain.model.login.LoginResponse

@Database(entities = [LoginResponse::class, Customers::class], version = 11, exportSchema = false)
@TypeConverters(Converters::class)
abstract class DatabaseManager : RoomDatabase() {
    abstract val userDao: UserDao
}

//abstract class DatabaseManager : RoomDatabase() {
//
//    abstract fun userDao(): UserDao
//
//    companion object {
//        // Singleton prevents multiple instances of database opening at the
//        // same time.
//        @Volatile
//        private var INSTANCE: DatabaseManager? = null
//
//        fun getDatabase(context: Context): DatabaseManager {
//            // if the INSTANCE is not null, then return it,
//            // if it is, then create the database
//            return INSTANCE ?: synchronized(this) {
//                val instance = Room.databaseBuilder(
//                    context.applicationContext,
//                    DatabaseManager::class.java,
//                    "user_database"
//                ).build()
//                INSTANCE = instance
//                // return instance
//                instance
//            }
//        }
//    }
//}