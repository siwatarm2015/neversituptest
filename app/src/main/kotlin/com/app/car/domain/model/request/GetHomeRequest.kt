package com.app.car.domain.model.request

import com.google.gson.annotations.SerializedName


data class GetHomeRequest(
    @SerializedName("token")
    var token: String = "",
    @SerializedName("longitude")
    var longitude: Double,
    @SerializedName("latitude")
    var latitude: Double,
    @SerializedName("radius")
    var radius: Int
)