package com.app.car.domain.model.request

import com.google.gson.annotations.SerializedName


data class CustomerDetailRequest(
    @SerializedName("token")
    var token: String = "",
    @SerializedName("customerId")
    var customerId: String = ""
)