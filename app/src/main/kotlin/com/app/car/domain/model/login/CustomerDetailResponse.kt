package com.app.car.domain.model.login


import com.google.gson.annotations.SerializedName

data class CustomerDetailResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("status")
    val status: Int
) {
    data class Data(
        @SerializedName("customerGrade")
        val customerGrade: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("isCustomerPremium")
        val isCustomerPremium: Boolean,
        @SerializedName("name")
        val name: String,
        @SerializedName("sex")
        val sex: String
    )
}