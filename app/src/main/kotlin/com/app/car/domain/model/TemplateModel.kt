package com.app.car.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TemplateModel(
    var msg: String = "",
    var data: String = ""
) : Parcelable
