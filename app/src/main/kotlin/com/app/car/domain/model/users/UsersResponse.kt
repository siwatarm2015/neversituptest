package com.app.car.domain.model.users

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user_table")
data class UsersResponse(
    @SerializedName("id")
    @PrimaryKey(autoGenerate = true)
    val userId: Int = 0,
    @SerializedName("address")
    @Embedded val address: Address = Address(),
    @SerializedName("company")
    @Embedded val company: Company = Company(),
    @SerializedName("email")
    val email: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("phone")
    val phone: String = "",
    @SerializedName("username")
    val username: String = "",
    @SerializedName("website")
    val website: String = ""
) {
    data class Address(
        @SerializedName("city")
        val city: String = "",
        @SerializedName("geo")
        @Embedded val geo: Geo = Geo(),
        @SerializedName("street")
        val street: String = "",
        @SerializedName("suite")
        val suite: String = "",
        @SerializedName("zipcode")
        val zipcode: String = ""
    ) {
        data class Geo(
            @SerializedName("lat")
            val lat: String = "",
            @SerializedName("lng")
            val lng: String = ""
        )
    }

    data class Company(
        @SerializedName("bs")
        val bs: String = "",
        @SerializedName("catchPhrase")
        val catchPhrase: String = "",
        @SerializedName("name")
        val companyName: String = ""
    )
}