import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "userdata_table")
data class LoginEntity(
    @PrimaryKey(autoGenerate = true)
    val dataId: Int = 0,
    @SerializedName("status")
    val status: Int,
    @SerializedName("token")
    val token: String,
    @SerializedName("customers")
    @Embedded val customers: MutableList<Customer>
) {
    data class Customer(
        @SerializedName("id")
        val userId: String,
        @SerializedName("name")
        val name: String
    )
}