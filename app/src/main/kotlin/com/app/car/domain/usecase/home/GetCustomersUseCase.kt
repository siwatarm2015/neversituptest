package com.app.car.domain.usecase.home

import com.app.car.data.repository.MainRepository
import com.app.car.domain.model.request.CustomerDetailRequest

class GetCustomersUseCase(private val mainRepository: MainRepository) {

    suspend fun logout(): Boolean =
        mainRepository.logout()

    suspend operator fun invoke(customerDetailRequest: CustomerDetailRequest) =
        mainRepository.getCustomerDetail(customerDetailRequest)

    suspend operator fun invoke() =
        mainRepository.getCustomer()

    suspend fun getUserToken() =
        mainRepository.getUserToken()
}