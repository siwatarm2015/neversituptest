package com.app.car.domain

sealed class StatusCode(val code: Int) {
    object SUCCESS : StatusCode(200)
    object POST_SUCCESS : StatusCode(201)
    object SERVER_ERROR : StatusCode(500)
    object UNAUTHORIZED : StatusCode(401)
    object BAD_REQUEST : StatusCode(400)
    object NOT_FOUND : StatusCode(404)
    object SERVICE_ERROR : StatusCode(503)
}