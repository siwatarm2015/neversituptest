package com.app.car.domain

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
</T> */

data class BaseResult<T>(val status: Status, val msg: String?, val data: T?) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun <T> success(data: T?): BaseResult<T> {
            return BaseResult(Status.SUCCESS, null, data)
        }
        fun <T> error(msg: String?): BaseResult<T> {
            return BaseResult(Status.ERROR, msg, null)
        }
        fun <T> loading(data: T? = null): BaseResult<T> {
            return BaseResult(Status.LOADING, null, data)
        }
    }
}

