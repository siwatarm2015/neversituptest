package com.app.car.domain.model.login

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "userdata_table")
data class LoginResponse(
    @PrimaryKey(autoGenerate = true)
    val dataId: Int = 0,
    @SerializedName("status")
    val status: Int,
    @SerializedName("token")
    val token: String,
    @SerializedName("customers")
    @Embedded val customers: ArrayList<Customers>
) {
    data class Customer(
        @PrimaryKey(autoGenerate = true)
        val cusId: Int = 0,
        @SerializedName("id")
        val userId: String,
        @SerializedName("name")
        val name: String
    )
}