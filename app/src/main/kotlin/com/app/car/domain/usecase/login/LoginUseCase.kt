package com.app.car.domain.usecase.login

import com.app.car.data.repository.MainRepository
import com.app.car.domain.model.request.LoginRequest

class LoginUseCase(private val mainRepository: MainRepository) {

    suspend fun login(loginRequest: LoginRequest) =
        mainRepository.login(loginRequest)

}