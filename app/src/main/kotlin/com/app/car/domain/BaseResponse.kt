package com.app.car.domain

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("code")
    val statusCode : Int = 0,
    @SerializedName("message")
    val msg : String = "",
    @SerializedName("data")
    val response : T?,
    @SerializedName("error")
    val error : String = ""
)