package com.app.car.domain

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
</T> */
sealed class Result<out T : Any> {
    data class Success<T : Any>(val data: T?) : Result<T>()
    data class Failure(val error: String?,val code: Int? = 0): Result<Nothing>()
    data class Loading(val isLoad: Boolean) : Result<Nothing>()
}
