package com.app.car.domain.model.login

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "customer_table")
data class Customers(
    @PrimaryKey(autoGenerate = true)
    val cusId: Int = 0,
    @SerializedName("id")
    val userId: String,
    @SerializedName("name")
    val name: String
)
