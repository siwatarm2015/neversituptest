package com.app.car.domain.dao

import androidx.room.*
import com.app.car.domain.model.login.Customers
import com.app.car.domain.model.login.LoginResponse
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(users: LoginResponse)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCustomer(customer: ArrayList<Customers>)

    @Insert
    fun insertUser(userEntityEntity: LoginResponse)

    @Update
    fun updateUser(userEntityEntity: LoginResponse)

    @Delete
    fun deleteUser(userEntityEntity: LoginResponse)

    @Query("SELECT * FROM userdata_table")
    fun getUserDataFromCache(): LoginResponse

    @Query("SELECT * FROM customer_table")
    fun getCustomer(): MutableList<Customers>

    @Query("SELECT token FROM userdata_table")
    fun getUserToken(): String

//    @Query("SELECT * FROM userdata_table WHERE userdata_table.id = :userId")
//    fun getUserById(userId: String): Flow<LoginResponse>

//    @Query("SELECT * FROM user_table")
//    fun getUserListFromCache(): MutableList<UsersResponse>

//    @Query("SELECT * FROM user_table WHERE user_table.userId = :userId")
//    fun getUserById(userId: String): UsersResponse

    @Query("DELETE FROM userdata_table")
    fun deleteUserTable()

    @Query("DELETE FROM customer_table")
    fun deleteCustomerTable()
}