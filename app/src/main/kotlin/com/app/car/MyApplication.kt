package com.app.car

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import coil.ImageLoader
import coil.ImageLoaderFactory
import coil.util.DebugLogger
import com.app.car.di.module
import com.app.car.utils.ResponseHeaderInterceptor
import com.orhanobut.hawk.Hawk
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger.*
import com.orhanobut.logger.PrettyFormatStrategy
import me.jessyan.autosize.AutoSizeConfig
import okhttp3.Cache
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber
import java.io.File

class MyApplication : Application() , ImageLoaderFactory {

    @SuppressLint("StaticFieldLeak")
    object C {
        lateinit var mContext: Context
    }

    override fun onCreate() {
        super.onCreate()

        C.mContext = this

        Hawk.init(this).build()

        startKoin {
            androidContext(this@MyApplication)
            modules(module)
            androidLogger()
        }

        AutoSizeConfig.getInstance().apply {
            isCustomFragment = false
            this.isBaseOnWidth = false
            isExcludeFontScale = true
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        PrettyFormatStrategy.newBuilder()
            .showThreadInfo(false) // (Optional) Whether to show thread info or not. Default true
            .methodCount(0) // (Optional) How many method line to show. Default 2
            .methodOffset(7) // (Optional) Hides internal method calls up to offset. Default 5
//                .logStrategy(customLog) // (Optional) Changes the log strategy to print out. Default LogCat
            .tag("custom") // (Optional) Global tag for every log. Default PRETTY_LOGGER
            .build().also {
                addLogAdapter(AndroidLogAdapter(it))
            }

    }


    override fun newImageLoader(): ImageLoader {
        return ImageLoader.Builder(this)
            .availableMemoryPercentage(0.5) // Use 50% of the application's available memory.
            .crossfade(true) // Show a short crossfade when loading images from network or disk into an ImageView.
            .okHttpClient {
                val cacheSize = 300 * 1024 * 1024
                // To create the an optimized Coil disk cache, use CoilUtils.createDefaultCache(context).
                val cacheDirectory = File(filesDir, "image_cache").apply { mkdirs() }
                val cache = Cache(cacheDirectory, cacheSize.toLong())

                // Rewrite the Cache-Control header to cache all responses for a year.
                val cacheControlInterceptor =
                    ResponseHeaderInterceptor("Cache-Control", "max-age=31536000,public")

                // Lazily create the OkHttpClient that is used for network operations.
                OkHttpClient.Builder()
                    .cache(cache)
                    .addNetworkInterceptor(cacheControlInterceptor)
                    .build()
            }.apply {
                // Enable logging to the standard Android log if this is a debug build.
                if (BuildConfig.DEBUG) {
                    logger(DebugLogger())
                }
            }
            .build()
    }

    companion object {
        fun getContext(): Context {
            return C.mContext
        }
    }
}
