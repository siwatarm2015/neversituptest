package com.app.car.extensions

import kotlinx.coroutines.*
import timber.log.Timber
import kotlin.math.max
import kotlin.system.measureTimeMillis

fun CoroutineScope.timer(
    interval: Long,
    fixedRate: Boolean = true,
    action: suspend TimerScope.() -> Unit
): Job {
    return launch {
        val scope = TimerScope()

        while (true) {
            val time = measureTimeMillis {
                try {
                    action(scope)
                } catch (ex: Exception) {
                    Timber.tag("LWTimer task").e(ex)
                }
            }

            if (scope.isCanceled) {
                break
            }

            if (fixedRate) {
                delay(max(0, interval - time))
            } else {
                delay(interval)
            }

            yield()
        }
    }
}

class TimerScope {
    var isCanceled: Boolean = false
        private set

    fun cancel() {
        isCanceled = true
    }
}

fun CoroutineScope.start(block: suspend CoroutineScope.() -> Unit) {
    launch(block = block)
}