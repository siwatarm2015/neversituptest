package com.app.car.extensions

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar

@SuppressLint("RestrictedApi")
fun BottomNavigationView.disableShiftingMode() {
    val menuView = this.getChildAt(0) as BottomNavigationMenuView
    try {
        val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
        shiftingMode.isAccessible = true
        shiftingMode.setBoolean(menuView, false)
        shiftingMode.isAccessible = false
        for (i in 0 until menuView.childCount) {
            val item = menuView.getChildAt(i) as BottomNavigationItemView
//            item.setShiftingMode(false)
            item.setChecked(item.itemData.isChecked)
        }
    } catch (e: NoSuchFieldException) {
        // ignored
    } catch (e: IllegalAccessException) {
        // ignored
    }
}

fun AppCompatButton.enableClick() {
    this.isEnabled = true
}

fun AppCompatButton.disableClick() {
    this.isEnabled = false
}

fun EditText.clear() {
    setText("")
}

fun AlertDialog.setFullWidth() {
    val lp = WindowManager.LayoutParams()
    lp.copyFrom(this.window?.attributes)
    lp.width = WindowManager.LayoutParams.MATCH_PARENT
    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
    this.window?.attributes = lp
}

fun AppCompatEditText.showKeyboard(activity: Activity) {
    if (requestFocus()) {
        (activity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
            .showSoftInput(this, SHOW_IMPLICIT)
    }
}

fun CoordinatorLayout.snackThat(
    message: CharSequence,
    buttonText: CharSequence,
    singleShot: View.OnClickListener?
) {
    val sb = Snackbar.make(this, message, Snackbar.LENGTH_INDEFINITE)
    if (singleShot != null) {
        sb.setAction(buttonText, singleShot)
    } else {
        sb.setAction(buttonText) { sb.dismiss() }
    }
    sb.show()
}

fun View.updateBackground(@DrawableRes idRes: Int) {
    this.background = ContextCompat.getDrawable(
        context!!,
        idRes
    )
}

//update text color
fun TextView.updateTextColor(@ColorRes idRes: Int) {
    this.setTextColor(
        ContextCompat.getColor(
            context!!,
            idRes
        )
    )
}

fun AppCompatButton.enable() {
    this.isEnabled = true
}

fun AppCompatButton.disable() {
    this.isEnabled = false
}

fun View.showSnackBar(
    message: String,
    length: Int = Snackbar.LENGTH_LONG,
    f: (Snackbar.() -> Unit) = {}
) {
    val snack = Snackbar.make(this, message, length)
    snack.f()
    snack.show()
}

fun View.showSnackBar(
    @StringRes message: Int,
    length: Int = Snackbar.LENGTH_LONG,
    f: (Snackbar.() -> Unit) = {}
) {
    showSnackBar(resources.getString(message), length, f)
}

fun RecyclerView.disableItemAnimator() {
    (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
}