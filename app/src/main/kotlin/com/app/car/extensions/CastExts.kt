package com.app.car.extensions


//Generic Cast or Null in Kotlin: reified + as?
inline fun <reified T> Any.genericCastOrNull(): T? {
    return this as? T
}


//Cast or Null in Kotlin: as?
fun castOrNull(anything: Any): String? {
    return anything as? String
}

//Smart Casting in Kotlin
fun checkBeforeCast(anything: Any) {
    if (anything is String) {
        println(anything.length)
    }
}