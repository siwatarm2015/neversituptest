package com.app.car.extensions

import android.graphics.Color
import androidx.annotation.ColorInt
import kotlin.math.roundToInt

@ColorInt
fun String.toColor(): Int {
    return Color.parseColor(this.trim())
}

@ColorInt
fun String.toColorWithAlpha(factor: Float): Int {
    val color = Color.parseColor(this.trim())
    val alpha = (Color.alpha(color) * factor).roundToInt()
    val red = Color.red(color)
    val green = Color.green(color)
    val blue = Color.blue(color)
    return Color.argb(alpha, red, green, blue)
}


@ColorInt
fun Int.setAlphaColor(factor: Float): Int {
    return Color.argb(
        (255 * factor).roundToInt(),
        Color.red(this),
        Color.green(this),
        Color.blue(this)
    )

}

